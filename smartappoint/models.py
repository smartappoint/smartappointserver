from django.db import models

# Create your models here.

class Specialty(models.Model):
    code = models.CharField(max_length=10)
    description = models.CharField(max_length=200)

    def __unicode__(self):
        return self.description

class Doctor(models.Model):
    register = models.CharField(max_length=20, unique=True)
    name = models.CharField(max_length=200)
    specialty = models.ForeignKey('Specialty')

    def __unicode__(self):
        return self.name

class Patient(models.Model):
    document = models.CharField(max_length=20, unique=True)
    name = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=13) #validate it through form (django-localflavor BRPhoneNumberField
    email = models.EmailField()
    address = models.CharField(max_length=200)

    def __unicode__(self):
        return self.name

class DoctorAddress(models.Model):
    doctor = models.ForeignKey('Doctor')
    address = models.CharField(max_length=200)
    description = models.CharField(max_length=200)
    email = models.EmailField()
    phone_number = models.CharField(max_length=13)

    class Meta:
        unique_together = ('doctor', 'address')

    def __unicode__(self):
        return u"%s: %s"%(self.doctor, self.description)

class DoctorAgenda(models.Model):
    doctor_address = models.ForeignKey('DoctorAddress')
    date = models.DateField()
    start = models.TimeField()
    end = models.TimeField()
    busy = models.BooleanField()

    class Meta:
        unique_together = ('doctor_address', 'date', 'start', 'end')

    def __unicode__(self):
        return u"%s: %s - %s - %s. Status: %s"%(self.doctor_address, self.date, self.start, self.end, ("Free", "Busy")[self.busy])

class Appointment(models.Model):
    doctor_address = models.ForeignKey('DoctorAddress')
    date = models.DateField()
    start = models.TimeField()
    end = models.TimeField()
    patient = models.ForeignKey('Patient')
    effective_start = models.TimeField(blank=True, null=True)

    class Meta:
        unique_together = ('doctor_address', 'date', 'start', 'end')

    def __unicode__(self):
        return u"%s: %s - %s - %s (%s)"%(self.patient, self.date, self.start, self.end, self.doctor_address)

class PatientDelay(models.Model):
    appointment = models.OneToOneField('Appointment')
    amount = models.TimeField()

class DoctorDelay(models.Model):
    appointment = models.OneToOneField('Appointment')
    amount = models.TimeField()
