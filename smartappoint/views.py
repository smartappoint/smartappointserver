from django.shortcuts import render
from django.http import HttpResponse

from rest_framework import status, viewsets
from rest_framework.decorators import api_view
from rest_framework.response import Response
from smartappoint.models import Doctor, Patient, Specialty, DoctorAddress, DoctorAgenda, Appointment, PatientDelay, DoctorDelay
from smartappoint.serializers import DoctorSerializer, PatientSerializer, SpecialtySerializer, DoctorAddressSerializer, DoctorAgendaSerializer, AppointmentSerializer, PatientDelaySerializer, DoctorDelaySerializer

class DoctorViewSet(viewsets.ModelViewSet):
    """
    List all doctors
    """
    queryset = Doctor.objects.all()
    serializer_class = DoctorSerializer

class SpecialtyViewSet(viewsets.ModelViewSet):
    """
    List all specitalties
    """
    queryset = Specialty.objects.all()
    serializer_class = SpecialtySerializer

class PatientViewSet(viewsets.ModelViewSet):
    """
    List all patients
    """
    queryset = Patient.objects.all()
    serializer_class = PatientSerializer

class DoctorAddressViewSet(viewsets.ModelViewSet):
    """
    List all addresses
    """
    queryset = DoctorAddress.objects.all()
    serializer_class = DoctorAddressSerializer

class DoctorAgendaViewSet(viewsets.ModelViewSet):
    """
    List all agendas
    """
    queryset = DoctorAgenda.objects.filter(busy=0).order_by('date', 'start')
    serializer_class = DoctorAgendaSerializer

class AppointmentViewSet(viewsets.ModelViewSet):
    """
    List all appointments
    """
    queryset = Appointment.objects.all()
    serializer_class = AppointmentSerializer

class PatientDelayViewSet(viewsets.ModelViewSet):
    """
    List all patients
    """
    queryset = PatientDelay.objects.all()
    serializer_class = PatientDelaySerializer

class DoctorDelayViewSet(viewsets.ModelViewSet):
    """
    List all patients
    """
    queryset = DoctorDelay.objects.all()
    serializer_class = DoctorDelaySerializer

class DoctorAgendaSearchViewSet(viewsets.ViewSet):
    """
    """

    def list(self, request):
        RAW_SQL = "select da1.* from smartappoint_doctoragenda da1 join smartappoint_doctoragenda da2 on da1.doctor_address_id = da2.doctor_address_id and da1.date <= da2.date and da1.start <= da2.start group by da1.doctor_address_id order by da1.date;"
        queryset = DoctorAgenda.objects.raw(RAW_SQL)
        serializer = DoctorAgendaSerializer(queryset, many=True, context={"request": request})
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = DoctorAgenda.objects.filter(doctor_address=pk).order_by('date', 'start')
        serializer = DoctorAgendaSerializer(queryset, many=True, context={"request": request})
        return Response(serializer.data)

class PatientAppointmentsViewSet(viewsets.ViewSet):
    """
    """

    def retrieve(self, request, pk=None):
        queryset = Appointment.objects.filter(patient=pk).order_by('date', 'start')
        serializer = DoctorAgendaSerializer(queryset, many=True, context={"request": request})
        return Response(serializer.data)