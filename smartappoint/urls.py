from django.conf.urls import url, include
from rest_framework import routers
from rest_framework.urlpatterns import format_suffix_patterns
from . import views

router = routers.DefaultRouter()
router.register(r'doctors', views.DoctorViewSet)
router.register(r'specialties', views.SpecialtyViewSet)
router.register(r'patients', views.PatientViewSet)
router.register(r'doctoraddress', views.DoctorAddressViewSet)
router.register(r'doctoragenda', views.DoctorAgendaViewSet)
router.register(r'appointment', views.AppointmentViewSet)
router.register(r'patientdelay', views.PatientDelayViewSet)
router.register(r'doctordelay', views.DoctorDelayViewSet)
router.register(r'doctoragendasearch', views.DoctorAgendaSearchViewSet, base_name="doctoragendasearch")
router.register(r'patientappointments', views.PatientAppointmentsViewSet, base_name="patientappointments")

urlpatterns = [
    url(r'^', include(router.urls)),
]
